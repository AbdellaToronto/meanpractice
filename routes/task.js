/**
 * Created with JetBrains WebStorm.
 * User: abdella
 * Date: 2013-08-28
 * Time: 6:37 PM
 * To change this template use File | Settings | File Templates.
 */


var mongoose = require('mongoose');
//var Tasks = mongoose.model('Task');

mongoose.connect( 'mongodb://localhost/taskList' );


var taskSchema = new mongoose.Schema({
    title: String,
    task: String
}, {strict: false});

var Tasks = mongoose.model( 'Task', taskSchema, "Task" );

var testObj = [{'title':'task1', 'task': 'first'}, {'title':'task2', 'task': 'second'}, {'title':'task3', 'task': 'third'}];


var db = mongoose.connection;


//console.log(testObj);



db.on('error', console.error.bind(console, 'connection error:'));



//GET List
exports.list = function(req, res){

    Tasks.find(function(err, tasks){

        if(err){
            return err;
        }
        res.send(tasks);
    });
};

//Handling POST

exports.post = function   (req, res){
    var newTask = new Tasks(req.body);

    newTask.save();


};


//delete item from list

exports.delete = function(req, res){
    console.log(req.query);

     Tasks.findById(req.query._id, function(err, task){
        console.log(task);
         task.remove();

     });

}