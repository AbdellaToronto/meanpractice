
/**
 * Module dependencies.
 */

var express = require('express');
//var db = require('./app/lib/db');
var task = require('./routes/task');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());

app.use(require('less-middleware')({ src: __dirname + '/app' }));
app.use(express.static(path.join(__dirname, 'app')));
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

//API for list of tasks
app.get('/task', task.list);
app.post('/task', task.post);
app.del('/task', task.delete);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
