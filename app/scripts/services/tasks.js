/**
 * Created with JetBrains WebStorm.
 * User: abdella
 * Date: 2013-08-28
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */


angular.module('testMeanApp')
    .factory('Task', function ($resource) {

        var task = $resource('task',{},{save:{method: 'POST', isArray:true}});


        return task;

    });