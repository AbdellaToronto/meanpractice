'use strict';

angular.module('testMeanApp', ['ngRoute', 'ngResource'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/taskList', {
        templateUrl: 'views/taskList.html',
        controller: 'TasklistCtrl'
      })
        .when('/task', {
            templateUrl:'views/taskList.html',
            controller: 'TaskListCtrl'
        })
      .otherwise({
        redirectTo: '/views/taskList.html'
      });
  });
